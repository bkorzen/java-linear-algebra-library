package LinearAlgebra;
import static org.junit.Assert.*;

/**Test for QE decomposition utility correctness
 * Based on test results obtained from WolframAlpha
 */
public class QRDecompositionTest {

    double[][] m = {
            {5, -2, 2, 7},
            {1, 0, 0, 3},
            {-3, 1, 5, 0},
            {3, -1, -9, 4}
    };
    Matrix matrix = new Matrix(m, m.length);
    QRDecomposition qr = new QRDecomposition(matrix);

    /**Test for Q matrix calculation correctness
     *
     */
    @org.junit.Test
    public void QMatrixCorrectness() {
        double[][] Qassert = {
                {-0.753778, -0.150756, 0.452267, -0.452267},
                {0.426401, -0.852803, 0.213201, -0.213201},
                {-0.471405, -0.471405 , -0.235702, 0.707107},
                {-0.166667, -0.166667, -0.83333, -0.5000}
        };
        Matrix QMatrix = qr.getQ();
        for(int i = 0; i < Qassert.length; ++i) {
            for ( int j = 0; j < Qassert.length; ++j)
                assertEquals(Qassert[i][j],QMatrix.matrix[i][j], 0.001);
        }
    }

    /**Test for R matrix calculation correctness
     *
     */
    @org.junit.Test
    public void RMatrixCorrectness() {
        double[][] Rassert = {
                {-6.63325,   2.41209,   4.82418,  -7.53778},
                {0.0000,  -0.426401,   3.83761,  -0.426401},
                {0.0000,   0.0000,  -8.48528,  -1.88562},
                {0.0000,   0.0000,   0.0000,   -3.66667}
        };
        Matrix RMatrix = qr.getR();

        for(int i = 0; i < Rassert.length; ++i) {
            for ( int j = 0; j < Rassert.length; ++j)
                assertEquals(Rassert[i][j],RMatrix.matrix[i][j], 0.001);
        }
    }

    /**Test if transposition of Q multiplied by R matrix is equal to original matrix
     *
     */
    @org.junit.Test
    public void matrixMatrixCorrectness() {
        Matrix q = qr.getQ();
        q.transpose();
        Matrix TestMatrix = Matrix.Multiply(q,qr.getR(),matrix.size);
        for(int i = 0; i < TestMatrix.size; ++i) {
            for ( int j = 0; j < TestMatrix.size; ++j)
                assertEquals(TestMatrix.matrix[i][j],matrix.matrix[i][j], 0.001);
        }
    }
}
