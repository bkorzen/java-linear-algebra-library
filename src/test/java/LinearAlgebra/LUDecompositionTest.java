package LinearAlgebra;

import static org.junit.Assert.assertEquals;

/**Test for LU decomposition utility correctness
 *
 */
public class LUDecompositionTest {

    double[][] m1 = {
            {7, 3, -11, 2},
            {-6, 7, 10, 3},
            {-11, 2, -2, 5},
            {-3, 1, -9, 4}
    };
    Matrix matrix = new Matrix(m1, m1.length);

    double[][] m2 = {
            {7, 3, -11},
            {-6, 7, 10},
            {-11, 2, -2}
    };

    Matrix matrix2 = new Matrix(m2, m2.length);

    /**Test for LU 4x4 matrix calculation correctness
     *
     */
    @org.junit.Test
    public void LUMatrix4x4Correctness() {
        Matrix LUassert = Matrix.Multiply(matrix.lu().getL(), matrix.lu().getU(), 4);

        for (int i = 0; i < LUassert.matrix.length; ++i) {
            for (int j = 0; j < LUassert.matrix.length; ++j)
                assertEquals(LUassert.matrix[i][j], matrix.matrix[i][j], 0.001);
        }
    }

    /**Test for LU 3x3 matrix calculation correctness
     *
     */

    @org.junit.Test
    public void LUMatrix3x3Correctness() {
        Matrix LUassert = Matrix.Multiply(matrix2.lu().getL(), matrix2.lu().getU(), 3);

        for (int i = 0; i < LUassert.matrix.length; ++i) {
            for (int j = 0; j < LUassert.matrix.length; ++j)
                assertEquals(LUassert.matrix[i][j], matrix2.matrix[i][j], 0.001);
        }
    }
}
