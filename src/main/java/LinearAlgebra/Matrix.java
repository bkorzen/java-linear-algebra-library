package LinearAlgebra;
/** Class for basic linear algebra calculations for square matrixes.
 * Class contains:
 * <ul>
 *     <li>constructors:
 *     <ul>
 *          <li>Empty square matrix with specified size.</li>
 *          <li>Square matrix from 2D array with specified size.</li>
 *          <li>Square matrix from 1D array with specified size.</li>
 *      </ul>
 *      </li>
 *      <li> public methods:
 *      <ul>
 *          <li>fillMatrix() - for filling matrix with random numbers;</li>
 *          <li>printMatrix() - for printing matrix to stdout.</li>
 *          <li>identity() - for overwriting current matrix by identity matrix</li>
 *          <li>clear() - for clearing current matrix (overwriting by 0);</li>
 *          <li>transpose() - for transposing matrix</li>
 *          <li>MultiplyByScalar() - multiply matrix by scalar</li>
 *      </ul>
 *      </li>
 *      <li>static methods:
 *      <ul>
 *          <li>Multiply() - multiply two matrixes</li>
 *          <li>Determinant() - calculate determinant of matrix</li>
 *          <li>Cofactor() - calculate cofactor matrix</li>
 *          <li>Adjugate() - calculate adjugate matrix</li>
 *          <li>Inverse() - calculate inverse matrix</li>
 *      </ul>
 *      </li>
 *      <li>decompositions:
 *      <ul>
 *          <li>lu() - calculate LU decomposition</li>
 *          <li>qr() - calculate QR decomposition</li>
 *          <li>cholesy() - calculate Cholesky decomposition</li>
 *      </ul>
 *      </li>
 * </ul>
 */
public class Matrix {

   /*------------------------------------
    Class variables
    ------------------------------------*/
    /**Dimension of matrix
     *
     */
    int size;
    /**Matrix
     *
     */
    public double[][] matrix;

    /** Get size of matrix
     *
     * @return matrix size
     */
    public int getSize() {
        return size;
    }

    /** Get matrix object
     *
     * @return Matrix
     */
    public double[][] getMatrix() {
        return matrix;
    }

    /*------------------------------------
    Constructors
    ------------------------------------*/

    /**Empty square matrix constructor
     *
     * @param size size of matrix
     */
    public Matrix( int size) {
        if( size < 1 ) throw new IllegalArgumentException("Size must be integer >=1!");
        this.size = size;
        matrix = new double[size][size];
    }

    /**Matrix from a 2D array
     *
     * @param m 2d array used for Matrix construction
     * @param size size of matrix
     */
    public Matrix ( double[][] m, int size ){
        if( size < 1 ) throw new IllegalArgumentException("Size must be integer >=1!");
        this.size = size;
        for(int i = 0; i < m.length;i++)
        {
            if(m.length != m[i].length | m.length != size) throw new IllegalArgumentException("Array size,dimensions not equal");
        }
        matrix =  Matrix.copyMatrix(m);
    }

    /**Matrix from 1D array
     *
     * @param m 1d array used to construct Matrix
     * @param size matrix size
     */
    public Matrix (double[] m, int size)
    {
        if( size < 1 | m.length != size*size ) throw new IllegalArgumentException("Size must be integer >=1, size^2 must be equal to 1d array count!");
        this.size = size;
        int iters = 0;
        matrix = new double[size][size];
        for(int i = 0 ; i < size ; i++)
        {
            for(int j = 0 ; j < size ; j++)
            {
                matrix[i][j]=m[iters];
                iters++;
            }
        }
    }

    /*------------------------------------
    Methods
    --------------------------------------*/

    /** fill matrix: for test purposes
     *
     */
    public void FillMatrix() {
        double A[][] = { {5, -2, 2, 7},
            {1, 0, 0, 3},
            {-3, 1, 5, 0},
            {3, -1, -9, 4}};
        for (int i = 0; i < size; ++i) {
            for (int j = 0; j < size; ++j)
                matrix[i][j] = A[i][j];
        }
    }

    /**print matrix to stdout
     *
     */
    public void printMatrix() {
        for (int i = 0; i < size; ++i) {
            for (int j = 0; j < size; ++j)
                System.out.printf("%5.2f ", matrix[i][j]);
            System.out.print("\n");
        }
        System.out.print("\n");
    }

    /**fill matrix: identity matrix
     *
     */
    public void identity() {
        for (int i = 0; i < matrix.length; ++i) {
            for (int j = 0; j < matrix[i].length; ++j) {
                matrix[i][j] = 0;
                if (i == j)
                    matrix[i][j] = 1;
            }
        }
    }

    /**clear matrix
     *
     */
    public void clear() {
        for (int i = 0; i < size; ++i) {
            for (int j = 0; j < size; ++j) {
                matrix[i][j] = 0;
            }
        }
    }

    /**method for transposing the matrix
     *
     */
    public void transpose() {
        for(int i = 0; i < size; ++i) {
            for(int j = i+1; j < size; ++j) {
                double temp = matrix[i][j];
                matrix[i][j] = matrix[j][i];
                matrix[j][i] = temp;
            }
        }
    }

    /** check if matrix is symmetric
     *
     * @return true
     */
    public boolean isSymmetric() {
        double[][] A = matrix;
        int N = A.length;
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < i; j++) {
                if (A[i][j] != A[j][i]) return false;
            }
        }
        return true;
    }

    /**method for multiplying matrix elements by a scalar
     *
     * @param scalar number; every index of matrix is multiplied by scalar
     */
    public void MultiplyByScalar (double scalar)  {
        for(int i = 0; i < size; ++i) {
            for (int j = 0; j < size; ++j) {
                matrix[i][j] = matrix[i][j]*scalar;
            }
        }
    }
    /*------------------------------------
    Static methods
    --------------------------------------*/
    /**method for multiplying two matrices
     *
     * @param matrix1 matrix on the left side
     * @param matrix2 matrix on the right side
     * @param size size od matrixes
     * @return matrix multiplication
     */
    public static Matrix Multiply(Matrix matrix1, Matrix matrix2, int size) {
        Matrix result = new Matrix(size);
        if (matrix2.size == size) {
            for(int i = 0; i < size; ++i) {
                for (int j = 0; j < size; ++j) {
                    for (int k = 0; k < size; ++k) {
                        result.matrix[i][j] += matrix1.matrix[i][k] * matrix2.matrix[k][j];
                    }
                }
            }
            return result;
        }
        else {
            System.out.println("Wrong matrix size!");
            return null;
        }
    }

    /**method for array multiplication (element with corresponding element)
     *
     * @param matrix1 matrix used to array multiplication
     */
    public void ArrayMultiplication(Matrix matrix1) {
        for(int i = 0; i < size; ++i) {
            for (int j = 0; j < size; ++j) {
                this.matrix[i][j] *=matrix1.matrix[i][j];
            }
        }
    }

    /**method for calculating determinant
     *
     * @param matrix matrix for determinant calculation
     * @param size size of matrix
     * @return determinant of matrix
     */
    public static double Determinant( Matrix matrix, int size) {
        int result = 0;
        if (size==1) return matrix.matrix[0][0];
        Matrix tempMatrix = new Matrix(size-1);
        int sign = 1;
        for (int i = 0; i < size; ++i) {
            Cofactor(matrix.matrix,tempMatrix.matrix,0,i,size);
            result+= sign*matrix.matrix[0][i]*Determinant(tempMatrix, size-1);
            sign = - sign;
        }
        return result;
    }

    /** private method for calculating cofactor matrix
     *
     * @param matrix matrix used to calculate cofactor matrix
     * @param temp temporary matrix
     * @param p row to ignore
     * @param q column to ignore
     * @param size size of matrix
     */
    private static void Cofactor( double matrix[][], double[][] temp, int p, int q,int size) {
        int i = 0, j = 0;
        for(int row = 0; row < size; ++row) {
            for(int col = 0; col < size; ++col) {
                if(row !=p && col != q) {
                    temp[i][j++] = matrix[row][col];
                    if(j == size -1) {
                        j = 0;
                        i++;
                    }
                }
            }
        }
    }

    /**method for calculating adjugate matrix
     *
     * @param matrix Matrix used to calculate adjugate matrix
     * @param size size of matrix
     * @return adjugate matrix
     */
    public static Matrix Adjugate(Matrix matrix, int size) {
        Matrix temp = new Matrix(matrix.size-1);
        Matrix adj = new Matrix(matrix.size);
        int sign = 1;
        if(size == 1) {
            adj.matrix[0][0] = 1;
            return adj;
        }
        for(int i = 0; i < size; ++i) {
            for(int j = 0; j < size; ++j) {
                Cofactor(matrix.matrix,temp.matrix,i,j,size);
                sign = ((i+j)%2 == 0)? 1 :-1 ;
                adj.matrix[j][i] = sign*Determinant(temp, size-1);
            }
        }
        return adj;
    }

    /**method for calculating inverse matrix
     *
     * @param matrix Matrix from which inverse matrix is calculated
     * @param size size of matrix
     * @return matrix inversion
     */
    public static Matrix Inverse(Matrix matrix, int size) {
        double det = Determinant(matrix, size);
        if (det == 0) {
            System.out.println("Singular matrix");
            return null;
        }
        Matrix adjoint = Adjugate(matrix, size);
        Matrix inverse = new Matrix(size);
        for (int i = 0; i < size; ++i) {
            for (int j = 0; j < size; ++j) {
                inverse.matrix[i][j] = adjoint.matrix[i][j] / det;
            }
        }
        return inverse;
    }
    /** Copying matrix content from one to another
     *
     * @param matrix Matrix to copy from
     * @return copy of matrix
     */
    public static double[][] copyMatrix(double[][] matrix) {
        double[][] myDouble = new double[matrix.length][];
        for (int i = 0; i < matrix.length; i++)
            myDouble[i] = matrix[i].clone();
        return myDouble;
    }

    /*------------------------------------
     Decompositions
     ------------------------------------*/

    /**method used to compute LU Deocmposition of a matrix
     *
     * @return LUDecomposition object containing L and U matrixes
     */
    public LUDecomposition lu()
    {
        return new LUDecomposition(this);
    }

    /**method used to compute QR Deocmposition of a matrix
     *
     * @return QRDecomposition object containing Q and R matrixes
     */
    public QRDecomposition qr()
    {
        return new QRDecomposition(this);
    }

    /**method used to compute Cholesky Deocmposition of a matrix
     *
     * @return CholeskyDecomposition object containing Ch matrix
     */
    public CholeskyDecomposition cholesky() { return new CholeskyDecomposition(this);}

}
