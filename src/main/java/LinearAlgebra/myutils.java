package LinearAlgebra;

/**Class used to store auxilliary methods
 *
 */
public class myutils {
    /** Copying matrix content from one to another
     *
     * @param matrix Matrix to copy from
     * @return copy of matrix
     */
    public static double[][] copyMatrix(double[][] matrix) {
        double[][] myDouble = new double[matrix.length][];
        for (int i = 0; i < matrix.length; i++)
            myDouble[i] = matrix[i].clone();
        return myDouble;
    }
}