/**Library for basic linear algebra calculations for square matrixes.
 * Library contains classes:
 * <ul>
 * <li>Matrix - constains class and static methods for matrix operations and allows to decompose matrix using LU, QR or Cholesky decomposition</li>
 * <li>LUDecomposition - used to calculate L and U matrixes based on given matrix</li>
 * <li>QRDecomposition - used to calculate Q and R matrixes based on given matrix</li>
 * <li>CholeskyDecomposition - used to calculate Cholesky decomposition based on given matrix</li>
 * <li>myutlis - collects auxiliary methods
 * </ul>
 */
package LinearAlgebra;