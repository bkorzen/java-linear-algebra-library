package LinearAlgebra;
/** Class allows to deompose matrix to two components: Q matrix and R matrix using householder's method.
 * QR decomposition (also called a QR factorization) of a matrix is a decomposition of a matrix A into a product A = QR of an orthogonal matrix Q and an upper triangular matrix R. QR decomposition is often used to solve the linear least squares problem, and is the basis for a particular eigenvalue algorithm, the QR algorithm.
 * Class contains:
 * <ul>
 *     <li>private fields:
 *          <ul>
 *              <li>Matrix Q - calculated Q component of matrix</li>
 *              <li>Matrix R - calculated R component of matrix</li>
 *              <li>Matrix QR - calculated QR from which Q and R matrixes ard calculated</li>
 *              <li>double[] RDiag - contains diagonal components of R matrix</li>
 *          </ul>
 *     </li>
 *     <li>constructor:
 *          <ul><li>With parameter Matrix</li></ul>
 *     </li>
 *     <li>public methods:
 *          <ul><li>getQ - returns Q matrix</li>
 *          <li>getR - returns R matrix</li></ul>
 *      </li>
 *      <li>private method:
 *          <ul><li>norm - calcualte square root of sum of two squared numbers without overflow or underflow</li></ul>
 *      </li>
 * </ul>
 */
public class QRDecomposition {
    private Matrix Q;
    private Matrix R;
    private Matrix QR;
    private double[] RDiag;

    /** Constructor to calculate Q matrix and R matrix
     * Constructor firstly computes QR matrix and diagonal values of R matrix.
     * Obtained values are used for Q matrix and R matrix computation.
     * @param m - Matrix used for QR decomposition
     */
    public QRDecomposition(Matrix m) {
        Q = new Matrix(m.size);
        R = new Matrix(m.size);
        QR = new Matrix(m.matrix, m.size);
        RDiag = new double[m.size];
        //Compute QR
        for (int k = 0; k < m.size; ++k) {

            double norm = 0;
            for (int i = k; i < m.size; ++i) {
                norm = norm(norm, QR.matrix[i][k]);
            }
            if (norm != 0) {
                if (QR.matrix[k][k] < 0)
                    norm = -norm;
                for (int i = k; i < m.size; ++i)
                    QR.matrix[i][k] /= norm;
                QR.matrix[k][k] += 1.0;

                for(int j = k+1; j < m.size; ++j) {
                    double temp = 0.0;
                    for (int i = k; i < m.size; ++i)
                        temp += QR.matrix[i][k]*QR.matrix[i][j];
                    temp = -temp/QR.matrix[k][k];
                    for (int i = k; i <m.size; ++i)
                        QR.matrix[i][j] += temp*QR.matrix[i][k];
                }
            }
            //Fill RDiag
            RDiag[k] = -norm;
        }

        //Compute Q
        for(int k = m.size-1; k >= 0; --k) {
            for (int i = 0; i < m.size; i++)
                Q.matrix[i][k] = 0.0;
            Q.matrix[k][k] = 1.0;
            for(int j = k; j < m.size; ++j) {
                if(QR.matrix[k][k] != 0) {
                    double temp = 0.0;
                    for(int i = k; i < m.size; ++i)
                        temp += QR.matrix[i][k]*Q.matrix[i][j];
                    temp = -temp/QR.matrix[k][k];
                    for(int i = k; i < m.size; ++i)
                        Q.matrix[i][j] += temp*QR.matrix[i][k];
                }
            }
        }
        Q.transpose();

        //Compute R
        for(int i = 0; i < m.size; ++i) {
            for(int j = 0; j < m.size; ++j) {
                if (i < j)
                    R.matrix[i][j] = QR.matrix[i][j];
                else if ( i == j )
                    R.matrix[i][j] = RDiag[i];
                else
                    R.matrix[i][j] = 0.0;
            }
        }

    }
    /** Get R matrix
     *
     * @return Matrix R
     */
    public Matrix getR() {
        return R;
    }

    /** Get Q matrix
     *
     * @return Matrix Q
     */
    public Matrix getQ() {
        return Q;
    }

    /** Square root sum of two squared numbers without overflow or underflow
     *
     * @param a first number
     * @param b second number
     * @return norm of two numbers
     */
    private static double norm(double a, double b) {
        double r;
        if (Math.abs(a) > Math.abs(b)) {
            r = b/a;
            r = Math.abs(a)*Math.sqrt(1+r*r);
        } else if (b != 0) {
            r = a/b;
            r = Math.abs(b)*Math.sqrt(1+r*r);
        } else {
            r = 0.0;
        }
        return r;
    }
}
