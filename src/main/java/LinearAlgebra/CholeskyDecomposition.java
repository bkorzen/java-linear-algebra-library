package LinearAlgebra;

/**Cholesky decomposition class*/

public class CholeskyDecomposition {

    /*------------------------------------
    Class variables
    ------------------------------------*/
    private Matrix Ch;

    /** Get Ch matrix
     *
     * @return Matrix Ch
     */
    public Matrix getCh(){
        return this.Ch;
    }

     /*------------------------------------
     Constructor
     ------------------------------------*/

    /** constructor computes Cholesky decomposition based on matrix A
     *
     * @param A matrix to decompose to Ch matrix
     */
    public CholeskyDecomposition(Matrix A){
        int N  = A.size;
        Ch = new Matrix(N);
        if (!A.isSymmetric()) {
            throw new RuntimeException("Matrix is not symmetric");
        }

        for (int i = 0; i < N; i++)  {
            for (int j = 0; j <= i; j++) {
                double sum = 0.0;
                for (int k = 0; k < j; k++) {
                    sum += Ch.matrix[i][k] * Ch.matrix[j][k];
                }
                if (i == j) Ch.matrix[i][i] = Math.sqrt(A.matrix[i][i] - sum);
                else        Ch.matrix[i][j] = 1.0 / Ch.matrix[j][j] * (A.matrix[i][j] - sum);
            }
            if (Ch.matrix[i][i] <= 0) {
                throw new RuntimeException("Matrix not positive definite");
            }
        }
    }

}
