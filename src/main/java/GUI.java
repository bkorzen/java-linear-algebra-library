import LinearAlgebra.CholeskyDecomposition;
import LinearAlgebra.LUDecomposition;
import LinearAlgebra.Matrix;
import LinearAlgebra.QRDecomposition;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class GUI implements ActionListener {

    private static int size;
    private static double[][] arrayMatrix;
    private static Matrix matrixObject;
    private static int result;
    private static JButton
            inverseBtn, multiplyBtn, scalarMultiplyBtn,
            determinantBtn, showMatrixBtn, transposingBtn,
            newMatrixBtn, identityBtn, luDecompositionBtn,
            qrDecompositionBtn, choleskyDecompositionBtn;
    private static JPanel[] choosePanel = new JPanel[8];

    GUI() {
        double[][] m = new double[4][4];
        matrixObject = new Matrix(m, m.length);
        matrixObject.FillMatrix();
        arrayMatrix = matrixObject.getMatrix();
        size = matrixObject.getSize();
        ChooseOperation();
    }

    private static void showMatrix(double[][] matrix, String title) {

        JPanel[] choosePanel = new JPanel[matrix.length + 1];
        choosePanel[0] = new JPanel();
        choosePanel[0].add(new JLabel(title));

        for (int i = 1; i <= matrix.length; i++) {
            choosePanel[i] = new JPanel();

            for (int j = 0; j < matrix[0].length; j++) {
                choosePanel[i].add(new JLabel(String.format("%.3f", matrix[i - 1][j])));

                if (j < matrix[0].length - 1) {
                    choosePanel[i].add(Box.createHorizontalStrut(15)); // spacing
                }
            }
        }

        if (size == 0) {
            JOptionPane.showMessageDialog(null, "You haven't entered any matrix");
        } else {
            JOptionPane.showMessageDialog(null, choosePanel, null, JOptionPane.PLAIN_MESSAGE, null);
        }

    }

    private static void showMatrix(double[][] matrix1, double[][] matrix2, String title1, String title2) {

        JPanel[] choosePanel1 = new JPanel[matrix1.length + 1];
        choosePanel1[0] = new JPanel();
        choosePanel1[0].add(new JLabel(title1));

        for (int i = 1; i <= matrix1.length; i++) {
            choosePanel1[i] = new JPanel();

            for (int j = 0; j < matrix1[0].length; j++) {
                choosePanel1[i].add(new JLabel(String.format("%.3f", matrix1[i - 1][j])));

                if (j < matrix1[0].length - 1) {
                    choosePanel1[i].add(Box.createHorizontalStrut(15)); // spacing
                }
            }
        }

        JPanel[] choosePanel2 = new JPanel[matrix2.length + 1];
        choosePanel2[0] = new JPanel();
        choosePanel2[0].add(new JLabel(title2));

        for (int i = 1; i <= matrix2.length; i++) {
            choosePanel2[i] = new JPanel();

            for (int j = 0; j < matrix2[0].length; j++) {
                choosePanel2[i].add(new JLabel(String.format("%.3f", matrix2[i - 1][j])));

                if (j < matrix2[0].length - 1) {
                    choosePanel2[i].add(Box.createHorizontalStrut(15)); // spacing
                }
            }
        }

        JPanel[] emptyPanel = new JPanel[matrix2.length + 1];
        emptyPanel[0] = new JPanel();
        emptyPanel[0].add(new JLabel(""));

        JPanel[][] choosePanel = new JPanel[3][];
        choosePanel[0] = choosePanel1;
        choosePanel[1] = emptyPanel;
        choosePanel[2] = choosePanel2;

        if (size == 0) {
            JOptionPane.showMessageDialog(null, "You haven't entered any matrix");
        } else {
            JOptionPane.showMessageDialog(null, choosePanel, null, JOptionPane.PLAIN_MESSAGE);
        }
    }

    private static void determinant() {
        if (arrayMatrix.length < 1) {
            JOptionPane.showMessageDialog(null, "You haven't entered any matrix");
        } else {
            double result = Matrix.Determinant(matrixObject, matrixObject.getSize());

            JOptionPane.showMessageDialog(null, String.format("det = %.3f",
                    result, null,
                    JOptionPane.PLAIN_MESSAGE, null));
        }
    }

    private static void transposing(double[][] matrix) {
        if (arrayMatrix.length < 1) {
            JOptionPane.showMessageDialog(null, "You haven't entered any matrix");
            return;
        }

        Matrix m = new Matrix(matrix, matrix.length);
        m.transpose();
        double[][] transposedMatrix = m.getMatrix();

        showMatrix(transposedMatrix, "Transposing Matrix");
    }

    private static void inverse() {
        double[][] inverseMatrix = Matrix.Inverse(matrixObject, matrixObject.getSize()).getMatrix();
        showMatrix(inverseMatrix, "Inversing Matrix");
    }

    private static void identity() {
        Matrix temp = new Matrix(arrayMatrix, arrayMatrix.length);
        temp.identity();
        double[][] identityMatrix = temp.getMatrix();
        showMatrix(identityMatrix, "Identity Matrix");
    }

    private static void multiplyByScalar() {

        double scalar;
        String tempString;

        if (arrayMatrix.length < 1) {
            JOptionPane.showMessageDialog(null, "You haven't entered any matrix");
            return;
        }

        tempString = JOptionPane.showInputDialog(null,
                "Enter a scalar number for multiplying");

        if (tempString == null) {
            return;
        } else if ((!tempString.equals(".")) && (isDouble(tempString) || isInt(tempString))) {
            scalar = Double.parseDouble(tempString);
        } else {
            JOptionPane.showMessageDialog(null, "You haven't entered a scalar");
            return;
        }

        Matrix temp = new Matrix(arrayMatrix, arrayMatrix.length);
        temp.MultiplyByScalar(scalar);
        double[][] m = temp.getMatrix();

        showMatrix(m, "Multiplication Result");
    }

    private static void multiplyByMatrix() {

        Matrix matrix2;
        Matrix multiplicationResult;

        if (arrayMatrix.length < 1) {
            JOptionPane.showMessageDialog(null, "You haven't entered any matrix");
        } else {
            double[][] m2 = new double[size][size];

            if (setElements(m2, "Fill multiplying matrix")) {

                matrix2 = new Matrix(m2, m2.length);
                multiplicationResult = Matrix.Multiply(matrixObject, matrix2, matrixObject.getSize());

                showMatrix(multiplicationResult.getMatrix(), "Multiplication Result");
            }
        }
    }

    private static void LUDecomposition() {
        showMatrix(matrixObject.lu().getL().getMatrix(), matrixObject.lu().getU().getMatrix(), "Matrix L", "Matrix U");
    }

    private static void QRDecomposition() {
        showMatrix(matrixObject.qr().getQ().getMatrix(), matrixObject.qr().getR().getMatrix(), "Matrix Q", "Matrix R");
    }

    private static void choleskyDecomposition() {
        if (!matrixObject.isSymmetric()) {
            JOptionPane.showMessageDialog(null, "Matrix isn't symmetric!");
            return;
        }

        /* example
        4   12 -16
        12  37 -43
        -16 -43 98
         */

        CholeskyDecomposition ch = new CholeskyDecomposition(matrixObject);
        double[][] temp = ch.getCh().getMatrix();
        for (int i = 0; i < temp.length; i++) {
            if (temp[i][i] <= 0 || !isDouble(Double.toString(temp[i][i]))) {
                JOptionPane.showMessageDialog(null, "Matrix isn't positively defined!");
                return;
            }
        }

        CholeskyDecomposition ch2 = new CholeskyDecomposition(matrixObject);
        ch2.getCh().transpose();
        showMatrix(ch.getCh().getMatrix(), ch2.getCh().getMatrix(), "Matrix Ch", "Transposed Matrix Ch");
    }

    private static boolean isInt(String string) {
        int temp;
        if (string.length() == '0')
            return false;

        for (temp = 0; temp < string.length(); temp++) {
            if (string.charAt(temp) != '+' && string.charAt(temp) != '-'
                    && !Character.isDigit(string.charAt(temp))) {
                return false;
            }
        }
        return true;
    }

    private static boolean isDouble(String string) {
        int temp;
        if (string.length() == '0')
            return false;

        for (temp = 0; temp < string.length(); temp++) {
            if (string.charAt(temp) != '+' && string.charAt(temp) != '-'
                    && string.charAt(temp) != '.'
                    && !Character.isDigit(string.charAt(temp))
                    ) {
                return false;
            }
        }
        return true;
    }

    private static void getDimension() {
        JTextField sizeField = new JTextField(5);

        JPanel choosePanel[] = new JPanel[2];
        choosePanel[0] = new JPanel();
        choosePanel[1] = new JPanel();
        choosePanel[0].add(new JLabel("Enter Size"));
        choosePanel[1].add(new JLabel("Size:"));
        choosePanel[1].add(sizeField);
        choosePanel[1].add(Box.createHorizontalStrut(15)); // spacing

        result = JOptionPane.showConfirmDialog(null, choosePanel,
                null, JOptionPane.OK_CANCEL_OPTION,
                JOptionPane.PLAIN_MESSAGE);

        int lastSize = size;

        if (result == 0) {

            if (sizeField.getText().equals(""))
                size = 0;
            else {
                if (isInt(sizeField.getText())) {
                    size = Integer.parseInt(sizeField.getText());
                } else {
                    JOptionPane.showMessageDialog(null, "Wrong Dimension");
                    size = lastSize;
                    return;
                }
            }
            if (size < 1) {
                JOptionPane.showConfirmDialog(null, "You entered wrong dimension",
                        "Error", JOptionPane.PLAIN_MESSAGE);
                size = lastSize;
            } else {
                double[][] tempMatrix = arrayMatrix;
                arrayMatrix = new double[size][size];
                if (!setElements(arrayMatrix, "Fill your new matrixObject")) {
                    arrayMatrix = tempMatrix;
                }
                matrixObject = new Matrix(arrayMatrix, arrayMatrix.length);
            }
        } else if (result == 1) {
            size = lastSize;
        }
    }

    private static void newMatrix() {
        getDimension();
    }

    private static boolean setElements(double matrix[][], String title) {
        String tempString;

        JPanel[] choosePanel = new JPanel[size + 2];
        choosePanel[0] = new JPanel();
        choosePanel[0].add(new Label(title));
        choosePanel[choosePanel.length - 1] = new JPanel();
        choosePanel[choosePanel.length - 1].add(new Label("Empty fields will be considered as zeros"));
        JTextField[][] inputField = new JTextField[matrix.length][matrix[0].length];

        for (int i = 1; i <= matrix.length; i++) {
            choosePanel[i] = new JPanel();

            for (int j = 0; j < matrix[0].length; j++) {
                inputField[i - 1][j] = new JTextField(3);
                choosePanel[i].add(inputField[i - 1][j]);

                if (j < matrix[0].length - 1) {
                    choosePanel[i].add(Box.createHorizontalStrut(15)); // spacing
                }
            }
        }

        result = JOptionPane.showConfirmDialog(null, choosePanel,
                null, JOptionPane.OK_OPTION, JOptionPane.PLAIN_MESSAGE);

        int lastSize = size;

        if (result == 0) {
            checkTextField(inputField);
            for (int i = 0; i < matrix.length; i++) {
                for (int j = 0; j < matrix[0].length; j++) {
                    tempString = inputField[i][j].getText();

                    if (isDouble(tempString)) {
                        matrix[i][j] = Double.parseDouble(inputField[i][j].getText());
                    } else {
                        JOptionPane.showMessageDialog(null, "You entered wrong elements");
                        size = lastSize;
                        return false;
                    }
                }
            }
            return true;
        } else
            return false;
    }

    private static void checkTextField(JTextField field[][]) {
        for (int temp = 0; temp < field.length; temp++) {
            for (int temp1 = 0; temp1 < field[0].length; temp1++) {
                if (field[temp][temp1].getText().equals(""))
                    field[temp][temp1].setText("0");
            }
        }
    }

    public static void main(String[] args) {
        GUI m1 = new GUI();
    }

    private void ChooseOperation() {
        int temp;

        for (temp = 0; temp < choosePanel.length; temp++) {
            choosePanel[temp] = new JPanel();
        }

//        choosePanel[1].add(Box.createHorizontalStrut(15)); // a spacer

//        choosePanel[6].add(Box.createHorizontalStrut(15)); // a spacer

        showMatrixBtn = new JButton("Show Matrix");
        showMatrixBtn.setPreferredSize(new Dimension(175, 35));
        showMatrixBtn.addActionListener(this);
        choosePanel[2].add(showMatrixBtn);

        multiplyBtn = new JButton("Multiply by matrix");
        multiplyBtn.setPreferredSize(new Dimension(175, 35));
        multiplyBtn.addActionListener(this);
        choosePanel[2].add(multiplyBtn);

        scalarMultiplyBtn = new JButton("Multiply by scalar");
        scalarMultiplyBtn.setPreferredSize(new Dimension(175, 35));
        scalarMultiplyBtn.addActionListener(this);
        choosePanel[2].add(scalarMultiplyBtn);

        transposingBtn = new JButton("Transposing");
        transposingBtn.setPreferredSize(new Dimension(175, 35));
        transposingBtn.addActionListener(this);
        choosePanel[3].add(transposingBtn);


        determinantBtn = new JButton("Determinant");
        determinantBtn.setPreferredSize(new Dimension(175, 35));
        determinantBtn.addActionListener(this);
        choosePanel[3].add(determinantBtn);

        inverseBtn = new JButton("Inversing");
        inverseBtn.setPreferredSize(new Dimension(175, 35));
        inverseBtn.addActionListener(this);
        choosePanel[3].add(inverseBtn);


        newMatrixBtn = new JButton("New Matrix");
        newMatrixBtn.setPreferredSize(new Dimension(175, 35));
        newMatrixBtn.addActionListener(this);
        choosePanel[4].add(newMatrixBtn);

        identityBtn = new JButton("Identity");
        identityBtn.setPreferredSize(new Dimension(175, 35));
        identityBtn.addActionListener(this);
        choosePanel[4].add(identityBtn);

        luDecompositionBtn = new JButton("LU Decomposition");
        luDecompositionBtn.setPreferredSize(new Dimension(175, 35));
        luDecompositionBtn.addActionListener(this);
        choosePanel[4].add(luDecompositionBtn);

        qrDecompositionBtn = new JButton("QR Decomposition");
        qrDecompositionBtn.setPreferredSize(new Dimension(175, 35));
        qrDecompositionBtn.addActionListener(this);
        choosePanel[5].add(qrDecompositionBtn);

        choleskyDecompositionBtn = new JButton("Cholesky Decomposition");
        choleskyDecompositionBtn.setPreferredSize(new Dimension(175, 35));
        choleskyDecompositionBtn.addActionListener(this);
        choosePanel[5].add(choleskyDecompositionBtn);


        JOptionPane.showConfirmDialog(null, choosePanel, null,
                JOptionPane.CLOSED_OPTION, JOptionPane.PLAIN_MESSAGE);

    }

    @Override
    public void actionPerformed(ActionEvent e) {

        if (e.getSource() == showMatrixBtn) {
            showMatrix(arrayMatrix, "Your Matrix");
        } else if (e.getSource() == multiplyBtn) {
            multiplyByMatrix();
        } else if (e.getSource() == inverseBtn) {
            inverse();
        } else if (e.getSource() == scalarMultiplyBtn) {
            multiplyByScalar();
        } else if (e.getSource() == transposingBtn) {
            transposing(arrayMatrix);
        } else if (e.getSource() == determinantBtn) {
            determinant();
        } else if (e.getSource() == newMatrixBtn) {
            newMatrix();
        } else if (e.getSource() == identityBtn) {
            identity();
        } else if (e.getSource() == luDecompositionBtn) {
            LUDecomposition();
        } else if (e.getSource() == qrDecompositionBtn) {
            QRDecomposition();
        } else if (e.getSource() == choleskyDecompositionBtn) {
            choleskyDecomposition();
        }
    }
}

